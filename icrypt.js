'use strict';

const algs = require('./algs');
var configArray = null;//require('./config');

function parseIsm(ism, config) {
  var parsedIsm = {
  };
	var ismBuckets = ism.split('//');

  if (ismBuckets.length > config.buckets.length)
    throw new Error("ism has more buckets than config; " + ismBuckets.length + " > " + config.buckets.length);

	for (let i = 0; i < ismBuckets.length; i++) {
    let bucket = config.buckets[i];
		parsedIsm[bucket] = ismBuckets[i].split('/');
	}

  return parsedIsm;
}

function generateKeySequence(ism, config) {
  var keySeq = [];
  var parsedIsm = parseIsm(ism, config);
  for (let i = 0; i < config.buckets.length; i++) {
    let bucket = config.buckets[i];
    if (!(bucket in parsedIsm))
      throw new Error("bucket " + bucket + " missing in ism");

    let arr = parsedIsm[bucket];
		arr.sort();
		
    for (let j = 0; j < arr.length; j++) {
      let tag = arr[j];
      if (tag === '-' || tag === '*') {
        keySeq.push({
          files: null,
          bucket: bucket,
          tag: tag,
          tagArrayIndex: -1
        });

        continue;
      }

			if (!(tag in config["keys"][bucket]))
      	throw new Error("tag " + tag + " missing in key configuration for bucket " + bucket);

			let files = config["keys"][bucket][tag];
      keySeq.push({
        files: files,
        bucket: bucket,
        tag: tag,
        tagArrayIndex: config.bucketInfo[bucket].tagToBucketTagArrayIndex[tag]
      });
    }
  }

  return keySeq;
}

function encrypt(ism, payload) {
  var keys = generateKeySequence(ism, configArray[0]);

  var encPayload = algs.encrypt({
    keys: keys,
    payload: payload,
    config: configArray[0]
  });

  return encPayload;
}

function decrypt(ism, payload) {
  var keys = generateKeySequence(ism, configArray[1]);

  var decPayload = algs.decrypt({
    keys: keys,
    payload: payload,
    config: configArray[1]
  });

  return decPayload;
}

module.exports = function(args) {
  configArray = require('./config')(args);

  return {
    encrypt: encrypt,
    decrypt: decrypt,
    configArray: configArray
  }
};
