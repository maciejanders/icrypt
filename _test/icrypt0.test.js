"use strict";

var fs = require('fs');
const NodeRSA = require('node-rsa');

var privkeyDogs = new NodeRSA(fs.readFileSync(__dirname + '/keys/classification/dogs/privkey.pem'));
var pubkeyDogs = new NodeRSA(fs.readFileSync(__dirname + '/keys/classification/dogs/pubkey.pem'));

describe('TEST CASE 1: VALID keys applied', () => {
	test('1x RSA encrypt, decrypt and check result', () => {
		var msg = "IT’S A SECRET TO EVERYBODY.";

		let enc = pubkeyDogs.encrypt(msg);
		let dec = privkeyDogs.decrypt(enc, 'utf8');

		// console.log(msgArgs.msg, dec.rcv1);

		expect(msg === dec).toBe(true);
	});
});
