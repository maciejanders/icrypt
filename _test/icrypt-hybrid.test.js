"use strict";

var icrypt = require('./icrypt-hybrid.js');
var fs = require('fs');
const NodeRSA = require('node-rsa');

// Dogs
var privkeyDogs = new NodeRSA(fs.readFileSync(__dirname + '/keys/classification/dogs/privkey.pem'));
var pubkeyDogs = new NodeRSA(fs.readFileSync(__dirname + '/keys/classification/dogs/pubkey.pem'));

// test file
var testFile = fs.readFileSync(__dirname + '/testFile.txt').toString();

describe('TEST CASE 1: VALID keys applied', () => {
	test('2x HYBRID encrypt, decrypt and check result', () => {
		let msgArgs = {
			payload: testFile,//"IT’S A SECRET TO EVERYBODY.",
			// payload: "abc",
			pubkey: pubkeyDogs
		};
		// console.log(msgArgs.payload.length);
		let enc1 = icrypt.encrypt(msgArgs);
		// console.log(enc1.payload.length);
	
		// var words = CryptoJS.enc.Base64.parse(enc.payload);
		// let t = CryptoJS.enc.Utf8.stringify(words);		
		// enc.payload = CryptoJS.enc.Utf8.stringify(words);		
		// console.log(enc.payload.length);
	
		enc1.pubkey = pubkeyDogs;
		enc1.payload = enc1.encSymKey + "." + enc1.payload;
		let enc2 = icrypt.encrypt(enc1);
		// console.log(enc2.payload.length);

		enc2.privkey = privkeyDogs;
		let dec2 = icrypt.decrypt(enc2);
		// console.log(dec2.payload.length);

		dec2.privkey = privkeyDogs;
		let arr = dec2.payload.split(".");
		dec2.payload = arr[1];
		dec2.encSymKey = arr[0];//enc1.encSymKey;
		let dec1 = icrypt.decrypt(dec2);
		// console.log(dec1.payload.length);

		expect(msgArgs.payload === dec1.payload).toBe(true);
	});
});
