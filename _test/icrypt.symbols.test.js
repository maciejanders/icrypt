"use strict";

// var fs = require('fs');
var icrypt = require('../icrypt')({ decFile: "icrypt.dec.2.config.json" });
var globals = require('../globals');

// test data
// var testFile = fs.readFileSync(__dirname + '/testFile.txt').toString();
var testText = "IT’S A SECRET TO EVERYBODY.";

describe('TEST CASE 1: "-" symbol: non-applicable decryption bucket', () => {

  test('enc config: 3 buckets, dec config: 4th bucket N/A', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "A//B//C";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "A//B//C//D";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload !== dec).toBe(true);
		}
	});

});

describe('TEST CASE 2: tag combinations ordered by size - helper function', () => {

  test('generate combinations of 4', () => {
    var orderedCombinations = globals.orderedCombinations(4);
	  expect(orderedCombinations[0].length === 4).toBe(true);
	  expect(orderedCombinations[1].length === 6).toBe(true);
	  expect(orderedCombinations[2].length === 4).toBe(true);
	  expect(orderedCombinations[3].length === 1).toBe(true);
  });
  
});

describe('TEST CASE 3: "*" symbol: missing information in bucket, VALID decryption keys', () => {

  test('enc config: 1st bucket 1-tag info missing', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "A//B//C";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "*//B//C//-";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload === dec).toBe(true);
		}
	});

  test('enc config: 3rd bucket 2-tag info missing', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "A//B//C/D";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "A//B//*//-";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload === dec).toBe(true);
		}
	});

  test('enc config: 2nd bucket 3-tag info missing', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "A//A/C/B//D";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "A//*//D//-";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload === dec).toBe(true);
		}
	});

  test('enc config: 1st/2nd bucket info missing', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "A//A/C/B//D";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "*//*//D//-";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload === dec).toBe(true);
		}
	});

  test('enc config: 1st/2nd/3rd bucket info missing', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "A//A/C/B//D";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "*//*//*//-";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload === dec).toBe(true);
		}
	});

});
