'use strict';

var CryptoJS = require("crypto-js");

// define the characters to pick from
var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz*&-%/!?*+=()";
// create a key for symmetric encryption
// pass in the desired length of your key
var generateKey = function generateKey(keyLength) {
	var randomstring = '';

	for (var i = 0; i < keyLength; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum, rnum + 1);
	}
	return randomstring;
};


module.exports = {
  encrypt: function(args) {
		var symKey = generateKey(50);
		var ciphertext = CryptoJS.AES.encrypt(args.payload, symKey);
		var encSymKey = args.pubkey.encrypt(symKey, 'base64');
		return {
			payload: ciphertext.toString(),
			encSymKey: encSymKey
		};
	},
	decrypt: function(args) {
		var symKey = args.privkey.decrypt(args.encSymKey, 'utf8');
		var bytes  = CryptoJS.AES.decrypt(args.payload, symKey);
		var plaintext = bytes.toString(CryptoJS.enc.Utf8);
		return {
			payload: plaintext
		};
	}
}
