"use strict";

var fs = require('fs');
var icrypt = require('../icrypt')();

// test file
var testFile = fs.readFileSync(__dirname + '/testFile.txt').toString();
var testText = "IT’S A SECRET TO EVERYBODY.";

describe('TEST CASE 1: encrypt long message, VALID keys applied', () => {
	test('encrypt, decrypt', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "A//B/C//D/E";
			var payload = testFile;
			var enc = icrypt.encrypt(ism, payload);
			ism = "A//B/C//D/E";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload === dec).toBe(true);
		}
	});
});

describe('TEST CASE 2: bulk encrypt, VALID keys applied', () => {
	test('encrypt, decrypt', () => {
		let i = 0;
		while (i++ < 100) {
			var ism = "A//B/C//D/E";
			var payload = testText;
			var enc = icrypt.encrypt(ism, payload);
			ism = "A//B/C//D/E";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload === dec).toBe(true);
		}
	});
});
