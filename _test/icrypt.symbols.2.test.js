"use strict";

// var fs = require('fs');
var icrypt = require('../icrypt')({ decFile: "icrypt.dec.3.config.json" });

// test data
// var testFile = fs.readFileSync(__dirname + '/testFile.txt').toString();
var testText = "IT’S A SECRET TO EVERYBODY.";

describe('TEST CASE 1: "*" symbol: missing information in bucket, INVALID decryption keys', () => {

  test('enc config: 1st bucket 1-tag info missing', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "A//B//E";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "*//B//E//-";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload !== dec).toBe(true);
		}
	});

  test('enc config: 3rd bucket 2-tag info missing', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "E//B//E/D";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "E//B//*//-";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload !== dec).toBe(true);
		}
	});

  test('enc config: 2nd bucket 3-tag info missing', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "E//A/C/B//E";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "E//*//E//-";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload !== dec).toBe(true);
		}
	});

});
