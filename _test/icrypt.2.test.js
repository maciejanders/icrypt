"use strict";

const fs = require('fs');
// var ursa = require('ursa');
const icrypt = require('../icrypt')();

// test file
var testFile = fs.readFileSync(__dirname + '/testFile.txt').toString();
var testText = "IT’S A SECRET TO EVERYBODY.";

describe('TEST CASE 1: encrypt long message, VALID keys applied', () => {
	test('encrypt, decrypt', () => {
		var ism = "A//B/C//D/E";
		var payload = testFile;
		var enc = icrypt.encrypt(ism, payload);
		ism = "A//B/C//D/E";
		let dec = icrypt.decrypt(ism, enc);

		expect(payload === dec).toBe(true);
	});
	test('encrypt, decrypt with switched 2nd bucket keys', () => {
    var config = icrypt.configArray[1];
		var ism = "A//B/C//D/E";
		var payload = testFile;
		var enc = icrypt.encrypt(ism, payload);
		ism = "A//C/B//D/E";
		let dec = icrypt.decrypt(ism, enc);

		expect(config.keys["scicontrols"]["B"] !== config.keys["scicontrols"]["C"]).toBe(true);
		expect(payload === dec).toBe(true);
	});
	test('encrypt, decrypt with switched 2nd/3rd bucket keys', () => {
    var config = icrypt.configArray[1];
		var ism = "A//B/C//D/E";
		var payload = testFile;
		var enc = icrypt.encrypt(ism, payload);
		ism = "A//C/B//E/D";
		let dec = icrypt.decrypt(ism, enc);

		expect(config.keys["scicontrols"]["B"] !== config.keys["scicontrols"]["C"]).toBe(true);
		expect(config.keys["disseminationcontrols"]["D"] !== config.keys["disseminationcontrols"]["E"]).toBe(true);
		expect(payload === dec).toBe(true);
	});
});

describe('TEST CASE 2: encrypt long message, INVALID keys applied', () => {
	test('encrypt, decrypt with key missing in 2nd bucket', () => {
		var ism = "A//B/C//D/E";
		var payload = testFile;
		var enc = icrypt.encrypt(ism, payload);
		ism = "A//B//D/E";
		let dec = icrypt.decrypt(ism, enc);

		expect(payload !== dec).toBe(true);
	});
	test('encrypt, decrypt with different 1st bucket key', () => {
    var config = icrypt.configArray[1];
		var ism = "A//B/C//D/E";
		var payload = testFile;
		var enc = icrypt.encrypt(ism, payload);
		ism = "B//B/C//D/E";
		let dec = icrypt.decrypt(ism, enc);

		expect(config.keys["classification"]["A"] !== config.keys["classification"]["B"]).toBe(true);
		expect(payload !== dec).toBe(true);
	});
	test('encrypt, decrypt with different 2nd bucket key', () => {
    var config = icrypt.configArray[1];
		var ism = "A//B/C//D/E";
		var payload = testFile;
		var enc = icrypt.encrypt(ism, payload);
		ism = "A//B/D//D/E";
		let dec = icrypt.decrypt(ism, enc);

		expect(config.keys["scicontrols"]["C"] !== config.keys["scicontrols"]["D"]).toBe(true);
		expect(payload !== dec).toBe(true);
	});
	test('encrypt, decrypt with different 3rd bucket key', () => {
    var config = icrypt.configArray[1];
		var ism = "A//B/C//D/E";
		var payload = testFile;
		var enc = icrypt.encrypt(ism, payload);
		ism = "A//B/C//C/E";
		let dec = icrypt.decrypt(ism, enc);

		expect(config.keys["disseminationcontrols"]["C"] !== config.keys["disseminationcontrols"]["D"]).toBe(true);
		expect(payload !== dec).toBe(true);
	});
});
