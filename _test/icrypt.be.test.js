"use strict";

var fs = require('fs');
var icrypt = require('../icrypt')();

// test data
// var testFile = fs.readFileSync(__dirname + '/testFile.txt').toString();
var testText = "IT’S A SECRET TO EVERYBODY.";

describe('TEST CASE 1: broadcast encrypt message, VALID keys applied', () => {
	test('100x encrypt with F, decrypt with A', () => {
		let i = 0;
		while (i++ < 100) {
			var ism = "A//B//F";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "A//B//A";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload === dec).toBe(true);
		}
	});

  test('encrypt with F, decrypt with B', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "A//B/C//F";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "A//B/C//B";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload === dec).toBe(true);
		}
  });

  test('encrypt with F, decrypt with C', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "A//B/C//F";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "A//B/C//C";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload === dec).toBe(true);
		}
	});

  test('encrypt with F in 2nd bucket, decrypt with C', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "A//F//D/E";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "A//C//D/E";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload === dec).toBe(true);
		}
	});

  test('encrypt with B and F in 2nd bucket, decrypt with B and C', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "A//B/F//D/E";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "A//B/C//D/E";
			let dec = icrypt.decrypt(ism, enc);

			expect(payload === dec).toBe(true);
		}
  });
  
  test('encrypt with B and F in 2nd bucket, decrypt with C and B', () => {
		let i = 0;
		while (i++ < 1) {
			var ism = "A//B/F//D/E";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "A//C/B//D/E";
			let dec = icrypt.decrypt(ism, enc);

      expect(payload === dec).toBe(true);
		}
	});

});

describe('TEST CASE 2: broadcast encrypt message, INVALID keys applied', () => {
  
  test('encrypt with F, decrypt with D', () => {
    let i = 0,
        config = icrypt.configArray[1]
    ;
		while (i++ < 1) {
			var ism = "A//B//F";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "A//B//D";
			let dec = icrypt.decrypt(ism, enc);

      expect(config.keys["disseminationcontrols"]["A"] !== config.keys["disseminationcontrols"]["D"]).toBe(true);
      expect(config.keys["disseminationcontrols"]["B"] !== config.keys["disseminationcontrols"]["D"]).toBe(true);
      expect(config.keys["disseminationcontrols"]["C"] !== config.keys["disseminationcontrols"]["D"]).toBe(true);
      expect(payload !== dec).toBe(true);
		}
	});

  test('encrypt with B and F in 2nd bucket, decrypt with A and C', () => {
    let i = 0,
        config = icrypt.configArray[1]
    ;
		while (i++ < 1) {
			var ism = "A//B/F//D/E";
			var payload = testText;
      var enc = icrypt.encrypt(ism, payload);
			ism = "A//A/C//D/E";
			let dec = icrypt.decrypt(ism, enc);

      expect(config.keys["scicontrols"]["A"] !== config.keys["scicontrols"]["B"]).toBe(true);
      expect(payload !== dec).toBe(true);
		}
	});
  
});
