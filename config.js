'use strict';

var fs = require('fs');
const NodeRSA = require('node-rsa');

function generate(configFileName) {
  var configArray = [];

  for (let type = 0; type < 2; type++) {
    if (!fs.existsSync(__dirname + configFileName[type]))
      throw new Error("filepath " + configFileName[type] + " does not exist in " + __dirname);

    let config = JSON.parse(fs.readFileSync(__dirname + configFileName[type]));
    configArray.push(config);

    config["bucketInfo"] = {};
    for (let i = 0; i < config.buckets.length; i++) {
      let bucket = config.buckets[i];
      if (!(bucket in config.keys))
        throw new Error("config bucket " + bucket + " missing in key configuration");

      config.bucketInfo[bucket] = {
        bucketToIndex: i,
        bucketTagArray: [],
        tagToBucketTagArrayIndex: {}
      };
    }

    for (let bucket in config.keys) {
      if (!config.keys.hasOwnProperty(bucket)) {
        continue;
      }
      if (!(bucket in config.bucketInfo))
        throw new Error("key bucket " + bucket + " missing in bucket configuration");

      let tag = "";
      for (tag in config.keys[bucket]) {
        if (!config.keys[bucket].hasOwnProperty(tag))
          continue;

        let files = config.keys[bucket][tag];
        // ignore broadcast encryption definitions
        if (Array.isArray(files)) {
          // keys must be in bucket
          for (let j = 0; j < files.length; j++) {
            if (!(files[j] in config.keys[bucket])) {
              throw new Error("keyring key " + files[j] + " missing in bucket " + bucket);
            }
          }

          // sort it to enforce order
          files.sort();

          continue;
        }

        if (type === 0 && !fs.existsSync(__dirname + files.pub))
          throw new Error("filepath " + files.pub + " does not exist in " + __dirname);

        if (type === 1 && !fs.existsSync(__dirname + files.prv))
          throw new Error("filepath " + files.prv + " does not exist in " + __dirname);

        let keysFromFiles = {
          prv: (type === 0 ? null : new NodeRSA(fs.readFileSync(__dirname + files.prv))),
          pub: (type === 1 ? null : new NodeRSA(fs.readFileSync(__dirname + files.pub)))
        };
        config["keys"][bucket][tag] = keysFromFiles;

        config.bucketInfo[bucket].bucketTagArray.push(tag);
      }

      config.bucketInfo[bucket].bucketTagArray.sort(); // enforce order
      for (let j = 0; j < config.bucketInfo[bucket].bucketTagArray.length; j++) {
        config.bucketInfo[bucket].tagToBucketTagArrayIndex[config.bucketInfo[bucket].bucketTagArray[j]] = j;
      }
    }
  }

  return configArray;
}

module.exports = function(args) {
  var configFileName = [
    "/" + (args && args.encFile || "icrypt.enc.config.json"), // encryption config
    "/" + (args && args.decFile || "icrypt.dec.config.json")  // decryption config
  ];
  
  return generate(configFileName);
}