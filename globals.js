"use strict";

var globals = {};

// make code work server/client side
!(function () {

  globals.isFunction = function(functionToCheck) {
    return functionToCheck && (
      {}.toString.call(functionToCheck) === '[object Function]'
      || {}.toString.call(functionToCheck) === '[object AsyncFunction]'
    );
  };

  globals.combinations = function(listSize){
    var set = [],
        combinationsCount = (1 << listSize);

    for (var i = 1; i < combinationsCount ; i++ , set.push(combination) )
        for (var j=0, combination = [];j<listSize;j++)
            if ((i & (1 << j)))
                combination.push(j);
    return set;
  };

  globals.orderedCombinations = function(listSize) {
    var set = globals.combinations(listSize),
        subsetsBySize = []
    ;
    for (let i = 0; i < listSize; i++) {
      subsetsBySize.push([]);
    }
    for (let i = 0; i < set.length; i++) {
      // // reverse order for decryption
      // set[i].sort((a, b) => b - a);
      subsetsBySize[set[i].length - 1].push(set[i]);
    }
    
    return subsetsBySize;
  }

  if (typeof module !== 'undefined' && module.exports) {
    module.exports = globals;
  }
})();
