'use strict';

// var fs = require('fs');
// var ursa = require('ursa');
var CryptoJS = require("crypto-js");
var globals = require('./globals');


// define the characters to pick from
var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz*&-%/!?*+=()";
// create a key for symmetric encryption
// pass in the desired length of your key
var generateKey = function generateKey(keyLength) {
	var randomstring = '';

	for (var i = 0; i < keyLength; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum, rnum + 1);
	}
	return randomstring;
};

function encrypt(args) {
  var payload = args.payload,
      i = 0
  ;
	try {
		for (i = 0; i < args.keys.length; i++) {
			let symKey = generateKey(50);

      let encSymKey = '';
      payload = CryptoJS.AES.encrypt(payload, symKey).toString();
      if (!Array.isArray(args.keys[i].files)) {
        encSymKey = args.keys[i].files.pub.encrypt(symKey, 'base64');
      } else {
        for (let j = 0; j < args.keys[i].files.length; j++) {
          let tag = args.keys[i].files[j];
          let bucket = args.keys[i].bucket;
          let key = args.config.keys[bucket][tag];
          let encSymKey0 = key.pub.encrypt(symKey, 'base64')
          if (encSymKey.length > 0)
            encSymKey += ',';
          encSymKey += encSymKey0;
        }
      }

      payload = encSymKey + "." + payload;
		}

		return payload;
	} catch (exc) {
		console.log("ERROR: enc iteration " + i, "payload length " + payload.length, exc.message);
		console.log(exc.message);
		return '';
	}
}

function decrypt(args) {
  var payload = args.payload,
      i = 0
  ;
	try {
		for (i = args.keys.length - 1; i >= 0; i--) {
      // ignore '-' marking
      if (args.keys[i].tag === '-')
        continue;

      let bucket = args.keys[i].bucket;
      let orderedCombinations = [[[args.keys[i].tagArrayIndex]]];
      if (args.keys[i].tag === '*') {
        let tabBucketCount = args.config.bucketInfo[bucket].bucketTagArray.length;
        orderedCombinations = globals.orderedCombinations(tabBucketCount);
      }

      let success = false;
      let originalPayload = payload;
      // for (let combinationLevel = 0; combinationLevel < orderedCombinations.length; combinationLevel++) {
      for (let combinationLevel = orderedCombinations.length - 1; combinationLevel >= 0; combinationLevel--) {
        for (let ocIndex = 0; ocIndex < orderedCombinations[combinationLevel].length; ocIndex++) {
          payload = originalPayload;
          // for (let combinationIndex = 0; combinationIndex < orderedCombinations[combinationLevel][ocIndex].length; combinationIndex++) {
          for (let combinationIndex = orderedCombinations[combinationLevel][ocIndex].length - 1; combinationIndex >= 0; combinationIndex--) {
            try  {
              let arr = payload.split(".");
              payload = arr[1];
              let encSymKeyArray = arr[0].split(",");
              let encSymKey = '';
              let symKey = '';
  
              let tagArrayIndex = orderedCombinations[combinationLevel][ocIndex][combinationIndex];
              let tag = args.config.bucketInfo[bucket].bucketTagArray[tagArrayIndex];
              let key = args.config["keys"][bucket][tag].prv;

              for (let k = 0; k < encSymKeyArray.length; k++) {
                encSymKey = encSymKeyArray[k];
                try {
                  symKey = key.decrypt(encSymKey, 'utf8');
                  // successful attempt - break out
                  success = true;
                  break;
                }
                catch (e) {
                  // failed attempt - move to the next encrypted sym key
                  success = false;
                }
              }
              if (!success)
                break;

              let bytes  = CryptoJS.AES.decrypt(payload, symKey);
              payload = bytes.toString(CryptoJS.enc.Utf8);

            } catch (e) {
              success = false;
              break;
            }
          }
          if (success)
            break;
        }
        if (success)
          break;
      }
		}

		return payload;
	} catch (exc) {
		console.log("ERROR: dec iteration " + i, "payload length " + (payload == null ? 0 : payload.length), exc.message);
		return '';
	}
}

module.exports = {
    encrypt: encrypt,
    decrypt: decrypt,
};
